from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
import telegram 
import logging
from time import sleep
import requests
import datetime
import re
import random  as  random_number
from googleplaces import GooglePlaces, types, lang

class SSLWSGIRefServer(bottle.ServerAdapter):
    def run(self, handler):
        from wsgiref.simple_server import make_server, WSGIRequestHandler
        import ssl
        if self.quiet:
            class QuietHandler(WSGIRequestHandler):
                def log_request(*args, **kw): pass
            self.options['handler_class'] = QuietHandler
        srv = make_server(self.host, self.port, handler, **self.options)
        srv.socket = ssl.wrap_socket (srv.socket,
        certfile='server.pem', server_side=True)
        srv.serve_forever()


TOKEN = '327633973:AAEdiWre0kG6S0fkDepfP1UZTw5GQxCByHQ'
APPNAME = 'SbbBot'

@route('/setWebhook')
def setWebhook():
    bot = telegram.Bot(TOKEN)
    botWebhookResult = bot.setWebhook(webhook_url='https://{}.azurewebsites.net/botHook'.format(\
APPNAME))
    return str(botWebhookResult)

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)

help_text = 'Type city where you are \nto get info about arrival station'

welcome_text = 'Hello, dear {_name}'

goodbye_text = 'Goodbye, dear {_name}'

start_col = 0


# Define a few command handlers. These usually take the two arguments bot and
# update. Error handlers also receive the raised TelegramError object in error.
def start(bot, update):
    global start_col
    if start_col > 2:
        update.message.reply_text(text='Stop kidding me. We have already salutated each other')
        start_col = 0
    else:
        update.message.reply_text(welcome_text.format(_name=update.message.from_user.first_name))
        start_col += 1


def help(bot, update):
    update.message.reply_text(help_text)

def bye(bot, update):
    update.message.reply_text(goodbye_text.format(_name=update.message.from_user.first_name))



def echo(bot, update):
    update.message.reply_text(update.message.text)


def error(bot, update, error):
    logger.warn('Update "%s" caused error "%s"' % (update, error))

def time_in_range(start, end, x):
    """Return true if x is in the range [start, end]"""
    if start <= end:
        return start <= x <= end
    else:
        return start <= x or x <= end

def get_pictures(name):
    ss = requests.Session()
    r = ss.get('https://yandex.ua/images/search?text='+name)
    p = 'div.class\=\"serp-item.*?url\"\:\"(.*?)\"'
    response = r.text
    w = re.findall(p,response)

    if len(w)>0:
        w = w[0:29:1]
        choice_f = random_number.choice(w)
    return choice_f

def get_coordinates(bot,name):
    searchAddress = name
    print (searchAddress)
    api_key = 'AIzaSyDmRGBZdLi1OhQGHO0AxSf4zpixD7cL2U0 '

    uri = 'https://maps.googleapis.com/maps/api/geocode/json?address=' + searchAddress + '&key=' + api_key

    result = requests.get(url=uri)
    if result.json()['status'] != 'ZERO_RESULTS':
        lat = result.json()['results'][0]['geometry']['location']['lat']
        lng = result.json()['results'][0]['geometry']['location']['lng']
        bot.send_location('332295873',lat,lng)

def get_details(city,name_place):
    YOUR_API_KEY = 'AIzaSyDmRGBZdLi1OhQGHO0AxSf4zpixD7cL2U0 '

    google_places = GooglePlaces(YOUR_API_KEY)

# You may prefer to use the text_search API, instead.
    query_result = google_places.nearby_search(
        location=city+', Switzerland', keyword=name_place, radius=20000)
# If types param contains only 1 item the request to Google Places API
# will be send as type param to fullfil:
# http://googlegeodevelopers.blogspot.com.au/2016/02/changes-and-quality-improvements-in_16.html
    if len(query_result.places)==0:
        return ''
    place = query_result.places[0]

# Returned places from a query are place summaries.

# The following method has to make a further API call.
    place.get_details()
# Referencing any of the attributes below, prior to making a call to
# get_details() will raise a googleplaces.GooglePlacesAttributeError.
#print place.details # A dict matching the JSON response from Google.
#print place.local_phone_number
#print place.international_phone_number
#print place.website
#print place.url

# Getting place photos
    photo_url = ''
    for photo in place.photos:
    # 'maxheight' or 'maxwidth' is required
        photo.get(maxheight=500, maxwidth=500)
    # MIME-type, e.g. 'image/jpeg'
        photo.mimetype
    # Image URL
        photo.url
    # Original filename (optional)
        photo.filename
    # Raw image data
        photo.data
        photo_url = photo.url
    return photo_url

#print photo.url



def arrival_dienste(bot,update,CITY):
    URL_OPEN_TIME_DIENSTE = 'https://data.sbb.ch/api/records/1.0/search/?dataset=haltestelle-offnungszeiten&rows=50&facet=stationsbezeichnung&facet=service&facet=periodevon&facet=periodebis&facet=von1&facet=bis1&facet=mo&facet=di&facet=mi&facet=do&facet=fr&facet=sa&facet=so&refine.stationsbezeichnung={_city}&apikey=6744c07e7acb2e90051b1d96c68dd0565d97295f8b6aab5f6b717b0f'.format(_city=CITY)
    r_time = requests.get(url=URL_OPEN_TIME_DIENSTE)
    hour, minutes = datetime.datetime.now().hour, datetime.datetime.now().minute
    today = datetime.datetime.now().strftime("%A")
    today_s = today
    services_print = ''

    eng_ger = {'Monday':'mo','Tuesday':'di','Wednesday':'mi','Thursday':'do','Friday':'fr','Saturday':'sa','Sunday':'so'}

    today = eng_ger[today]
    services = {}


    CITY = CITY.replace(' ','') 
    CITY = CITY.replace('+','')
    if r_time.json()['nhits']>1:
        update.message.reply_text(text='At the station {station} there are several services:\n'.format(station=CITY))
    elif r_time.json()['nhits']==1:
        update.message.reply_text(text='At the station {station} there is onle one service:\n'.format(station=CITY))
    else:
        update.message.reply_text(text='At the station {station} there is no services.\n'.format(station=CITY))

     
    for i in range(r_time.json()['nhits']):
        bot.sendChatAction(chat_id=update.message.chat_id, action=telegram.ChatAction.TYPING)
        if r_time.json()['records'][i]['fields']['service'] not in services.keys():
            services[r_time.json()['records'][i]['fields']['service']] = 'Closed'
        for field in r_time.json()['records'][i]['fields']:
            if field == today and r_time.json()['records'][i]['fields'][field]==1:
                von = r_time.json()['records'][i]['fields']['von1']
                bis = r_time.json()['records'][i]['fields']['bis1']
                hours_von, minutes_von = map(int, von.split(':'))
                hours_bis, minutes_bis = map(int, bis.split(':'))
                start = datetime.time(hours_von,minutes_von,0)
                end = datetime.time(hours_bis,minutes_bis,0)
                if time_in_range(start, end, datetime.time(hour,minutes,0)):
                    services[r_time.json()['records'][i]['fields']['service']] = 'Opened'
    for j,i in enumerate(services.keys()):
        services_print += '{num}) '.format(num=j+1)+i+' : '+services[i] + '\n' 
    update.message.reply_text(text = services_print+'\n'+get_pictures('SBB')) 
        #update.message.reply_text(text='{num}) '.format(num=j+1)+i+' : '+services[i] + '( Work time for '+today_s+' is : '+von+' - '+bis+')')
        

def arrival_betriebe(bot,update,CITY):
    URL_SHOPES = 'https://data.sbb.ch/api/records/1.0/search/?dataset=nebenbetriebe&rows=40&facet=stationsbezeichnung&facet=nebenbetrieb&refine.stationsbezeichnung={_city}&apikey=6744c07e7acb2e90051b1d96c68dd0565d97295f8b6aab5f6b717b0f'.format(_city=CITY)
    r_shop = requests.get(url=URL_SHOPES)
    #day = datetime.datetime.now().strftime("%A, %d. %B %Y %I:%M%p") 
    #hour , minutes = datetime.datetime.now().strftime("%I"),datetime.datetime.now().strftime("%M") 

    CITY = CITY.replace(' ','') 
    CITY = CITY.replace('+','')   
    if r_shop.json()['nhits']>1:
        update.message.reply_text(text='At the station {station} there are several shops:\n'.format(station=CITY))
    elif r_shop.json()['nhits']==1:
        update.message.reply_text(text='At the station {station} there is onle one shop:\n'.format(station=CITY))
    else:
        update.message.reply_text(text='At the station {station} there is no shops.\n'.format(station=CITY))
    for i in range(r_shop.json()['nhits']):
        bot.sendChatAction(chat_id=update.message.chat_id, action=telegram.ChatAction.TYPING)
        #update.message.reply_text(text='{num}) '.format(num=i+1)+r_shop.json()['records'][i]['fields']['nebenbetrieb']+' ('+r_shop.json()['records'][i]['fields'].get('bemerkungen','no comments')+')')
        zeit = r_shop.json()['records'][i]['fields'].get('oeffnungszeiten', '0')
        serv = r_shop.json()['records'][i]['fields']['nebenbetrieb'].replace(' ','')
        name = serv+'%20'+CITY+'%20Bahnhof'
        if zeit != '0':
            update.message.reply_text(text='{num}) '.format(num=i+1)+r_shop.json()['records'][i]['fields']['nebenbetrieb']+' ('+r_shop.json()['records'][i]['fields'].get('bemerkungen','no comments')+')'+'\n'+'Work time: '+zeit+'\n' +get_details(CITY,serv))
            get_details(CITY,serv)
            get_coordinates(bot,name)
            #update.message.reply_text(text='Work time: '+zeit+'\n'+get_pictures(name))
        else:
            update.message.reply_text(text='{num}) '.format(num=i+1)+r_shop.json()['records'][i]['fields']['nebenbetrieb']+' ('+r_shop.json()['records'][i]['fields'].get('bemerkungen','no comments')+')'+'\n'+'Work time: '+'no information \n'+get_details(CITY,serv))
            get_details(CITY,serv)
            get_coordinates(bot,name)
            #update.message.reply_text(text='Work time: '+ 'no information \n'+get_pictures(name))
        sleep(2)


def arrival_wifi(bot,update,CITY):
    URL_WIFI = 'https://data.sbb.ch/api/records/1.0/search/?dataset=wifistation&rows=79&facet=standort&apikey=6744c07e7acb2e90051b1d96c68dd0565d97295f8b6aab5f6b717b0f'

    r_wifi = requests.get(url=URL_WIFI)

    stations = []
    for i in range(len(r_wifi.json()['records'])): 
        stations.append(r_wifi.json()['records'][i]['fields']['standort'].lower())

    if CITY.lower() in stations:
        text = 'will be'
    else:
        text = 'will not be'
    
    update.message.reply_text(text='At the station {station} you {access} provided with free wifi access'.format(station=CITY,access=text))

def departure_print(bot,update,city):
    flag = False
    city = city[0].upper()+city[1:]
    for i in range(1,len(city)):
        if city[i].isupper():
            flag = True
            break
    if flag:
        city = city[:i]+' '+city[i:]
        residue = city[i+1:]
        arrival_wifi(bot,update,city)
        if (residue=='Sbb'):
            arrival_betriebe(bot,update,city[:i]+'+'+residue.upper())
            arrival_dienste(bot,update,city[:i]+'+'+residue.upper())

        else:
            arrival_betriebe(bot,update,city[:i]+'+'+residue)
            arrival_dienste(bot,update,city[:i]+'+'+residue)
    else:
        arrival_wifi(bot,update,city)
        arrival_betriebe(bot,update,city)
        arrival_dienste(bot,update,city)

    
def arrival_print(bot,update,city):
    flag = False
    city = city[0].upper()+city[1:]
    for i in range(1,len(city)):
        if city[i].isupper():
            flag = True
            break
    if flag:
        city = city[:i]+' '+city[i:]
        residue = city[i+1:]
        arrival_wifi(bot,update,city)
        if (residue=='Sbb'):
            arrival_betriebe(bot,update,city[:i]+'+'+residue.upper())
            arrival_dienste(bot,update,city[:i]+'+'+residue.upper())

        else:
            arrival_betriebe(bot,update,city[:i]+'+'+residue)
            arrival_dienste(bot,update,city[:i]+'+'+residue)
    else:
        arrival_wifi(bot,update,city)
        arrival_betriebe(bot,update,city)
        arrival_dienste(bot,update,city) 
    



def analyse_text(bot, update):
    msg = update.message.text
    msg_array = msg.split()
    arrival=''
    
    welcome = ["hi","hello", "hey", "bot"]
    farewell = ["bye","goodbye","tchus","chao"]
    if msg.lower() in welcome:
        start(bot,update)
        return

    if msg.lower() in farewell:
        bye(bot,update)
        return
    
    if set(msg_array) & set(welcome):
        start(bot,update)
        sleep(1)
        if 'from' in msg_array:
            index = msg_array.index('from')
            arrival_print(bot,update,msg_array[index+1])
        return 


    if len(msg_array)==1 and msg_array not in welcome:
        arrival_print(bot,update,msg_array[0])
        return
    elif 'from' in msg_array:
        index = msg_array.index('from')
        arrival_print(bot,update,msg_array[index+1])
        if 'to' in msg_array:
            index2 = msg_array.index('to')
            departure_print(bot,update,msg_array[index2+1])
        return
    elif len(msg_array) == 2:
        departure_print(bot,update,msg_array[0])
        arrival_print(bot,update,msg_array[1])
        return
        

def main():
    # Create the EventHandler and pass it your bot's token.
    updater = Updater("327633973:AAEdiWre0kG6S0fkDepfP1UZTw5GQxCByHQ")

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    # on different commands - answer in Telegram
    dp.add_handler(CommandHandler("start", start))
    dp.add_handler(CommandHandler("help", help))
    dp.add_handler(CommandHandler("bye", bye))


    # on noncommand i.e message - echo the message on Telegram
    dp.add_handler(MessageHandler(Filters.text, analyse_text))

    # log all errors
    dp.add_error_handler(error)

    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()

if __name__ == '__main__':
    app = bottle.Bottle()
    srv = SSLWSGIRefServer(host="sbbbot.azurewebsites.net", port=80)
    bottle.run(server=srv)

#if __name__ == '__main__':
#    main()
